call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', {'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'dracula/vim'

Plug 'tpope/vim-sensible'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-ruby/vim-ruby'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'Quramy/tsuquyomi'
Plug 'elixir-lsp/coc-elixir', {'do': 'yarn install && yarn prepack'}
Plug 'elixir-editors/vim-elixir'

Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }
Plug 'jparise/vim-graphql'
Plug 'preservim/nerdcommenter'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'prettier/vim-prettier', {
\ 'do': 'yarn install',
\ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

Plug 'rust-lang/rust.vim'
Plug 'airblade/vim-gitgutter'
"Plug 'tpope/vim-rails'
"Plug 'tpope/vim-bundler'
"Plug 'tpope/vim-dispatch'

" Plug 'HerringtonDarkholme/yats.vim'
Plug 'yuezk/vim-js'
Plug 'peitalin/vim-jsx-typescript'
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'

call plug#end()

if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" Color scheme related
syntax on
set t_Co=256
set cursorline
colorscheme dracula
let g:airline_theme='onehalfdark'
highlight CocFloating ctermbg=black
highlight CocErrorFloat ctermfg=white
" lightline
" let g:lightline = { 'colorscheme': 'onehalfdark' }

set background=dark
set termencoding=utf-8
set encoding=utf-8
set number
set mouse-=a
set tabstop=2
set softtabstop=2
set shiftwidth=2
set noexpandtab
let g:airline_powerline_fonts=1

" https://github.com/neoclide/coc.nvim/wiki/Completion-with-sources
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()

" Ruby
let g:coc_global_extensions = ['coc-solargraph', 'coc-tsserver', 'coc-json', 'coc-rust-analyzer', 'coc-html-css-support', 'coc-css', 'coc-sql', 'coc-yaml']

autocmd Filetype typescript setlocal ts=2 sw=2 expandtab
autocmd Filetype html setlocal ts=2 sw=2 expandtab
autocmd Filetype javascript setlocal ts=2 sw=2 expandtab
au BufNewFile,BufRead *.prisma setfiletype graphql

" Run:
" :CocInstall coc-json coc-tsserver coc-sourcekit

" Rust
let g:rustfmt_autosave = 1

let g:NERDTreeShowHidden = 0
let g:NERDTreeMinimalUI = 0
let g:NERDTreeIgnore = ['node_modules', 'target']
let NERDTreeStatusLine='NERDTree'
" Automaticaly close nvim if NERDTree is only thing left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Toggle
nnoremap <silent> <C-a> :NERDTreeToggle<CR>
